#!/bin/zsh

# Need to add /usr/local/bin to PATH
PATH=/usr/local/bin:$PATH

BREW=brew

if ! command -v $BREW &> /dev/null
then
    echo "ERROR: $BREW not found" >&2
    exit 1
fi

$BREW update >/dev/null
OUTDATED_F=$($BREW outdated | wc -l)
OUTDATED_C=$($BREW outdated --cask | wc -l)
OUTDATED=$((OUTDATED_F+OUTDATED_C))

if [[ $OUTDATED -gt 0 ]]; then
	MSG="You have $OUTDATED outdated packeges\nPlease check your homebrew installation"
	osascript -e "display notification \"$MSG\" with title \"Homebrew\""
fi
