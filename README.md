# README #

### Summary

This project contains two files:

* `brew-check-update.sh` A simple script to check whether you have outdated homebrew packages
* `brew-check-update.plist` A property list file to create a job that automates the check and notifies the logged in user

### Setup

1. Clone the repo
2. Copy `brew-check-update.sh` to `/usr/local/bin` or somewhere else in your PATH
3. Copy `brew-check-update.plist` to `/Library/LaunchAgents' 
4. Load the property list file:

  `launchctl bootstrap gui/$(id -u) /Library/LaunchAgents/brew-check-update.plist`

5. Wait until 9:00 AM :P

